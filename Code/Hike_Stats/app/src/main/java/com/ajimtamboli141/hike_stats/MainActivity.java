package com.ajimtamboli141.hike_stats;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.material.textview.MaterialTextView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    MaterialTextView displayAddress, displayCity, displayState, displayCountry, displayPostalCode, displayLandmark;

    ArrayList<Stats> stats_ref = new ArrayList<>();

    private LineChart chart1; // reference of class LineChart
    private LineChart chart2; // reference of class LineChart

    // Location values
    double latitude;
    double longitude;

    // Reverse geocoding
    Geocoder geocoder;
    List<Address> addresses;

//    2002.44789
//    2002.44789 - 2000 =
//            20
//
//            02.44789 / 60 = 0.04079
//    Above result + 20
//    Latitude : 20.040798167
//
//            07428.52201
//            74
//
//            28.52201 / 60 = 0.475366833
//    Above result + 74
//    Longitude : 74.475366833
//
//            20.0407 N 74.4753 E

    private double convertNmeaLat(double latitude) {
        int degreeWhole = (int) latitude / 100;

        double x = latitude % 1000;

        if(x >= 100)
            x = x % 100;

        return ((x / 60) + degreeWhole);
    }

    private double convertNmeaLong(double longitude) {
        int degreeWhole = (int) longitude / 100;

        double x = longitude % 100;

        return ((x / 60) + degreeWhole);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayAddress = findViewById(R.id.displayAddress);
        displayCity = findViewById(R.id.displayCity);
        displayState = findViewById(R.id.displayState);
        displayCountry = findViewById(R.id.displayCountry);
        displayPostalCode = findViewById(R.id.displayPostalCode);
        displayLandmark = findViewById(R.id.displayLandmark);

        // Chart initialization / config
        setupChart();
        setupChartAltitude();

        // try to receive a broadcasted message
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // this method will be called when a notification (remote message)
                // will be received by the device

                // call the API to get the latest temperature values
                getData(null);
            }
        };

        // register for the required broadcast message
        IntentFilter intentFilter = new IntentFilter("com.ajimtamboli141.hike_stats.NEW_STATS_DATA");
        registerReceiver(receiver, intentFilter);
    }

    private void setupChart() {
        chart1 = findViewById(R.id.chart1);

        // background color
        chart1.setBackgroundColor(Color.WHITE);

        // disable description text
        chart1.getDescription().setEnabled(false);

        // enable touch gestures
        chart1.setTouchEnabled(true);

        // set listeners
        chart1.setDrawGridBackground(false);

        chart1.setPinchZoom(false);

        XAxis xAxis;
        {   // // X-Axis Style // //
            xAxis = chart1.getXAxis();

            // vertical grid lines
            xAxis.enableGridDashedLine(10f, 10f, 0f);
        }

        YAxis yAxis;
        {   // // Y-Axis Style // //
            yAxis = chart1.getAxisLeft();

            // disable dual axis (only use LEFT axis)
            chart1.getAxisRight().setEnabled(false);

            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f);

            // axis range
            yAxis.setAxisMaximum(60f);
            yAxis.setAxisMinimum(0f);
        }


        {   // // Create Limit Lines // //
            LimitLine llXAxis = new LimitLine(9f, "Index 10");
            llXAxis.setLineWidth(4f);
            llXAxis.enableDashedLine(10f, 10f, 0f);
            llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            llXAxis.setTextSize(10f);

            LimitLine ll1 = new LimitLine(150f, "Upper Limit");
            ll1.setLineWidth(4f);
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);

            LimitLine ll2 = new LimitLine(-30f, "Lower Limit");
            ll2.setLineWidth(4f);
            ll2.enableDashedLine(10f, 10f, 0f);
            ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            ll2.setTextSize(10f);

            // draw limit lines behind data instead of on top
            yAxis.setDrawLimitLinesBehindData(true);
            xAxis.setDrawLimitLinesBehindData(true);

            // add limit lines
            yAxis.addLimitLine(ll1);
            yAxis.addLimitLine(ll2);
            //xAxis.addLimitLine(llXAxis);
        }

        chart1.animateX(1500);

        // get the legend (only possible after setting data)
        Legend l = chart1.getLegend();

        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE);
    }

    private void renderLineChart(){
        // data for the line chart
        ArrayList<Entry> values = new ArrayList<>();

        for (int index = 0; index < stats_ref.size(); index++) {
            Stats stats = stats_ref.get(index);
            values.add(new Entry(index, stats.getTemperature()));
        }

        LineDataSet set1 = new LineDataSet(values, "Temperature");

        set1.setDrawIcons(false);

        // draw dashed line
        set1.enableDashedLine(10f, 5f, 0f);

        // black lines and points
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);

        // line thickness and point size
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);

        // draw points as solid circles
        set1.setDrawCircleHole(false);

        // customize legend entry
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);

        // text size of values
        set1.setValueTextSize(9f);

        // draw selection line as dashed
        set1.enableDashedHighlightLine(10f, 5f, 0f);

        // set the filled area
        set1.setDrawFilled(true);
        set1.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return chart1.getAxisLeft().getAxisMinimum();
            }
        });

        // set color of filled area
        if (Utils.getSDKInt() >= 18) {
            // drawables only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_blue);
            set1.setFillDrawable(drawable);
        } else {
            set1.setFillColor(Color.BLACK);
        }

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the data sets

        // create a data object with the data sets
        LineData data = new LineData(dataSets);

        // set data
        chart1.setData(data);

        // refresh chart
        chart1.invalidate();
    }

    private void setupChartAltitude() {
        chart2 = findViewById(R.id.chart2);

        // background color
        chart2.setBackgroundColor(Color.WHITE);

        // disable description text
        chart2.getDescription().setEnabled(false);

        // enable touch gestures
        chart2.setTouchEnabled(true);

        // set listeners
        chart2.setDrawGridBackground(false);

        chart2.setPinchZoom(true);

        XAxis xAxis;
        {   // // X-Axis Style // //
            xAxis = chart2.getXAxis();

            // vertical grid lines
            xAxis.enableGridDashedLine(10f, 10f, 0f);
        }

        YAxis yAxis;
        {   // // Y-Axis Style // //
            yAxis = chart2.getAxisLeft();

            // disable dual axis (only use LEFT axis)
            chart2.getAxisRight().setEnabled(false);

            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f);

            // axis range
            yAxis.setAxisMaximum(1000f);
            yAxis.setAxisMinimum(0f);
        }


        {   // // Create Limit Lines // //
            LimitLine llXAxis = new LimitLine(9f, "Index 10");
            llXAxis.setLineWidth(4f);
            llXAxis.enableDashedLine(10f, 10f, 0f);
            llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            llXAxis.setTextSize(10f);

            LimitLine ll1 = new LimitLine(1500f, "Upper Limit");
            ll1.setLineWidth(4f);
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);

            LimitLine ll2 = new LimitLine(-30f, "Lower Limit");
            ll2.setLineWidth(4f);
            ll2.enableDashedLine(10f, 10f, 0f);
            ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            ll2.setTextSize(10f);

            // draw limit lines behind data instead of on top
            yAxis.setDrawLimitLinesBehindData(true);
            xAxis.setDrawLimitLinesBehindData(true);

            // add limit lines
            yAxis.addLimitLine(ll1);
            yAxis.addLimitLine(ll2);
            //xAxis.addLimitLine(llXAxis);
        }

        chart2.animateX(1500);

        // get the legend (only possible after setting data)
        Legend l = chart2.getLegend();

        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE);
    }

    private void renderLineChartAltitude(){
        // data for the line chart
        ArrayList<Entry> values = new ArrayList<>();

        for (int index = 0; index < stats_ref.size(); index++) {
            Stats stats = stats_ref.get(index);
            values.add(new Entry(index, stats.getAltitude()));
        }

        LineDataSet set1 = new LineDataSet(values, "Altitude");

        set1.setDrawIcons(false);

        // draw dashed line
        set1.enableDashedLine(10f, 5f, 0f);

        // black lines and points
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);

        // line thickness and point size
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);

        // draw points as solid circles
        set1.setDrawCircleHole(false);

        // customize legend entry
        set1.setFormLineWidth(1f);
        set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        set1.setFormSize(15.f);

        // text size of values
        set1.setValueTextSize(9f);

        // draw selection line as dashed
        set1.enableDashedHighlightLine(10f, 5f, 0f);

        // set the filled area
        set1.setDrawFilled(true);
        set1.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return chart2.getAxisLeft().getAxisMinimum();
            }
        });

        // set color of filled area
        if (Utils.getSDKInt() >= 18) {
            // drawables only supported on api level 18 and above
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_blue);
            set1.setFillDrawable(drawable);
        } else {
            set1.setFillColor(Color.BLACK);
        }

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the data sets

        // create a data object with the data sets
        LineData data = new LineData(dataSets);

        // set data
        chart2.setData(data);

        // refresh chart
        chart2.invalidate();
    }

    public void getData(View v) {

        Ion.with(this)
                .load("http://192.168.43.142:4000/stats")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.e("MainActivity", result.toString());

                        // Get status value from JsonObject result
                        String status = result.get("status").getAsString();

                        // Check if the data was successfully received
                        if(status.equals("success")){
                            // Remove the old data from the array
                            stats_ref.clear();

                            // Get the temperatures array from the JsonObject result
                            JsonArray data = result.get("data").getAsJsonArray();

                            // Iterate over the JsonArray data and get the objects from it
                            for(int index = 0; index < data.size(); index++){
                                // Get the JSON object at index'th position
                                JsonObject object = data.get(index).getAsJsonObject();

                                // Get the values from the JsonObject object and set it into a object of Stats class
                                Stats stats = new Stats();
                                stats.setId(object.get("id").getAsInt());
                                stats.setTime(object.get("time").getAsInt());
                                stats.setLatitude(object.get("latitude").getAsDouble());
                                stats.setNsh(object.get("nsh").getAsString());
                                stats.setLongitude(object.get("longitude").getAsDouble());
                                stats.setEwh(object.get("ewh").getAsString());
                                stats.setFixQuality(object.get("fixQuality").getAsInt());
                                stats.setConnectedSats(object.get("connectedSats").getAsInt());
                                stats.setSpeed(object.get("speed").getAsFloat());
                                stats.setDate(object.get("date").getAsInt());
                                stats.setPressure(object.get("pressure").getAsFloat());
                                stats.setTemperature(object.get("temperature").getAsFloat());
                                stats.setAltitude(object.get("altitude").getAsFloat());

                                // Add to ArrayList of stats_ref
                                stats_ref.add(stats);

                                // Convert NMEA Format Lat and Long to suitable format for reverse geocoding
                                if(index == 0){
                                    latitude = convertNmeaLat(stats.getLatitude());
                                    longitude = convertNmeaLong(stats.getLongitude());

                                    // Get the location details
                                    geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                                    try {
                                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                    } catch (IOException ioException) {
                                        ioException.printStackTrace();
                                    }

                                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                    String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String country = addresses.get(0).getCountryName();
                                    String postalCode = addresses.get(0).getPostalCode();
                                    String landmark = addresses.get(0).getFeatureName(); // Only if available else return NULL

                                    // Set the textViews with the latest values of Location
                                    displayAddress.setText(address);
                                    displayCity.setText(city);
                                    displayState.setText(state);
                                    displayCountry.setText(country);
                                    displayPostalCode.setText(postalCode);
                                    displayLandmark.setText(landmark);
                                }

//                                Log.e("stats", String.valueOf(stats.getTime()));
//                                Log.e("stats", String.valueOf(stats.getTemperature()));
                            }

                            // Render line chart with the newly received data
                            renderLineChart();
                            renderLineChartAltitude();
                        }
                        else{
                            Toast.makeText(MainActivity.this, "Error in getting data from the python server.", Toast.LENGTH_SHORT);
                        }
                    }
                });
    }
}