package com.ajimtamboli141.hike_stats;


import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class AppMessagingService extends FirebaseMessagingService{
    // Get new token and send to python server
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        Log.e("AppMessagingService", "TOKEN: "+s);
    }

    // Whenever google server send a new message, we will handle it via onMessageReceived
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if(remoteMessage.getNotification() != null){
            Log.e("AppMessagingService", remoteMessage.getNotification().getTitle());
            Log.e("AppMessagingService", remoteMessage.getNotification().getBody());

            Intent intent = new Intent();
            intent.setAction("com.ajimtamboli141.hike_stats.NEW_STATS_DATA");
            intent.putExtra("stats_data", remoteMessage.getNotification().getBody());
            sendBroadcast(intent);
        }
    }
}