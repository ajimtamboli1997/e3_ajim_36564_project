# sudo pip3 install flask

from flask import Flask, request, jsonify
import mysql.connector
from pyfcm import FCMNotification

# Create a push service instance
# Grab the api key from console.firebase.google.com project settings
# TODO
push_service = FCMNotification(api_key="AAAAD3it4nI:APA91bEPgJFHb9mPzPnFWOC_0dIFWyiHmxKY9sEWKP_CXa7teFGXTS9feDsvCD5uNNLF3h0rNdgg730dI-9vLAvG2Wp8Ha5ZRRBeSfvd1K-DRAvS6JoJEwiVickzlVcie5IP98YIVoRE")

# create a server process
app = Flask(__name__)


def open_connection():
  connection = mysql.connector.connect(host="localhost", user="root", password="Root", database="gps_db", port=3306)
  
  return connection


def execute_select_query(query):
  """
  this function is supposed to execute select query
  and return the output
  """
  connection = open_connection()
  
  cursor = connection.cursor()
  cursor.execute(query)

  data = cursor.fetchall()

  cursor.close()
  connection.close()
  
  return data


def execute_query(query):
  """
  this function is supposed to execute insert, update
  and delete queries and commit the connection
  """
  connection = open_connection()
  cursor = connection.cursor()
  cursor.execute(query)
  connection.commit()

  cursor.close()
  connection.close()


def create_response(data, error=None):
  # create empty dictionary
  result = dict()

  # check if there is any error
  if error == None:
    # there is no error to report
    result['status'] = 'success'
    result['data'] = data

  else:
    # there is an error 
    result['status'] = 'error'

  return jsonify(result)
    

@app.route("/", methods=["GET"])
def welcome():
  return "Location and Environment Stats"


@app.route("/stats", methods=["GET"])
def get_stats():
  # query = "select id, temperature, collection_on from temperature_data order by id desc limit 10"
  #query = "select id, time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude from gps_table order by id desc limit 10"
  query = "select id, time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude from gps_table order by id desc limit 10"


  gps_table = execute_select_query(query)
  print(gps_table)

  gps_values = []
  for (id, time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude) in gps_table:
    gps_values.append({
      # "id": id,
      # "temperature": temperature,
      # "collection_on": collection_on
      "id": id,
      "time": time,
      "latitude": latitude,
      "nsh": nsh,
      "longitude": longitude,
      "ewh": ewh,
      "fixQuality": fixQuality,
      "connectedSats": connectedSats,
      "status": status,
      "speed": speed,
      "date": date,
      "pressure": pressure,
      "temperature": temperature,
      "altitude": altitude
    })

  return create_response(gps_values)


@app.route('/stats', methods=["POST"])
def post_stats():
  # get the temperature value sent by NodeMCU
  time = request.json.get('time')
  latitude = request.json.get('latitude')
  nsh = request.json.get('nsh')
  longitude = request.json.get('longitude')
  ewh = request.json.get('ewh')
  fixQuality = request.json.get('fixQuality')
  connectedSats = request.json.get('connectedSats')
  status = 'V'
  speed = request.json.get('speed')
  date = request.json.get('date')
  pressure = request.json.get('pressure')
  temperature = request.json.get('temperature')
  altitude = request.json.get('altitude')
  
  #query = f"insert into gps_table (time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude) values ('{time}', '{latitude}', '{nsh}', '{longitude}', '{ewh}', '{fixQuality}', '{connectedSats}', '{status}', '{speed}', '{date}', '{pressure}', '{temperature}', '{altitude}')"
  query = f"insert into gps_table (time,latitude,nsh,longitude,ewh,fixQuality, connectedSats,status,speed,date,pressure,temperature,altitude) values ('{time}','{latitude}','{nsh}','{longitude}','{ewh}','{fixQuality}','{connectedSats}','{status}','{speed}','{date}','{pressure}','{temperature}','{altitude}')"
  execute_query(query)

  # TODO
  # registration_id = "dcMAJAzoCos:APA91bFaERTMbxjQp-ZKJZz9ZBsfQWU5ALpimBrYWY8JVIx-fE0P2ki9CW4pMsxKp1cJnDsmIvwfTX-FFuM5zd6vwOhAbdH49FtxunINhzSaAy7AFvuugJrrFMtX6lPxzyYAhLnk__r9"
  # message_title = "New GPS Stats"
  
  # Below commented code doesn't work
  # # Save new data in a dictionary
  # data = {
  #   "temperature": temperature
  # }
  # # Jsonify the dictonary and assign it to the message body
  # message_body = jsonify(data)

  # TODO
  # message_body = f"stats: {stats}"
  # result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)

  # print(f"result: {result}")
  
  return create_response('added stats') 


# start the server
app.run(host="0.0.0.0", debug=True, port=4000)
