#include "Wire.h" 
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

// wifi credentials
const char *ssid = "Redmi7";
const char *password = "123456780";

SoftwareSerial s(D6,D5);

typedef struct gps_data{
    int time;
    double latitude;
    char NSH; // Northern or Southern Hemisphere
    double longitude;
    char EWH; // Eastern or Western Hemisphere
    int fixQuality;
    int connectedSats; // Number of satellites connected
    char status;
    double speed;
    int date;
}gps_data_t;

typedef struct env_values{
  float pressure;
  float temperature;
  float altitude;
}env_t;

typedef enum gps_field{
    TIME = 1, LATITUDE = 2, NSH = 3, LONGITUDE = 4, EWH = 5, QUALITY = 6, NUMSV = 7, SPEED = 8, DATE = 9, PRESSURE = 10, TEMPERATURE = 11, ALTITUDE = 12
}gps_field_t;

char buff [256];
volatile byte indx;
static volatile int receive_data_flag;
static volatile int tokenization_flag;
static volatile int loop_count;
gps_data_t location;
env_t env_val;

void setup() {
  Serial.begin(9600);
  Serial.flush();

  // set the WiFi mode to Station Mode
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  Serial.println("Connecting to WiFi");

  // wait till the wifi gets connected
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  // nodeMCU got connected
  Serial.println("WiFi connected...");
  Serial.print("IP Address of NodeMCU: ");
  Serial.println(WiFi.localIP());
}

// Serial read GPS String
void get_gps_str(){
  byte c = 'a';
  while (c != '\n') {
    if (s.available() > 0){    
      c = s.read();
      if (indx < sizeof buff) {
        buff [indx++] = c; // add received character to buff
        //check for end of string
        if (c == '\n') {
          receive_data_flag = 1;
          indx= 0; //reset index
        }
      } 
    }
  }
}

void tokenize() {

  char *token;
  int i;

  Serial.println("Tokenizing...");
  
  token = strtok(buff, "⸮?,");
  location.time = atoi(token);
  
  i = 2;
  
  while(tokenization_flag == 0) {
    
        token = strtok(NULL, ",\r\n");

        switch (i)
        {
        case LATITUDE:
            location.latitude = strtod(token, NULL);
            break;

        case NSH:
            location.NSH = *token;
            break;

        case LONGITUDE:
            location.longitude = strtod(token, NULL);
            break;

        case EWH:
            location.EWH = *token;
            break;

        case QUALITY:
            location.fixQuality = atoi(token);
            break;

        case NUMSV:
            location.connectedSats = atoi(token);
            break;

        case SPEED:
            location.speed = atof(token);
            break;

        case DATE:
            location.date = atoi(token);
            break;

        case PRESSURE:
            env_val.pressure = atof(token);
            break;

        case TEMPERATURE:
            env_val.temperature = atof(token);
            break;

        case ALTITUDE:
            tokenization_flag = 1;
            env_val.altitude = atof(token);
            break;

        default:
            Serial.println("Invalid case.\n");
            break;
        }

        i++;
    }
    Serial.println("Tokenization ended");
}

void display_tokenize_data() {
  Serial.println("Time: " + String(location.time) + " Lat: " + String(location.latitude) + " NSH: " + String(location.NSH) + " Long: " + String(location.longitude) + " EWH: " + String(location.EWH) + " FixQ: " + String(location.fixQuality) + " ConSats: " + String(location.connectedSats) + " Spd: " + String(location.speed) + " Dt: " + String(location.date) + " Pr: " + String(env_val.pressure) + " Tmp: " + String(env_val.temperature) + " Alt: " + String(env_val.altitude));
}

void loop() {
  s.begin(9600);
  loop_count++;
  Serial.println("loop_count: " + String(loop_count));
  
  get_gps_str();
  Serial.print(buff);
  Serial.println(strlen(buff));

  if(receive_data_flag == 1){
    tokenize();
    display_tokenize_data();
    tokenization_flag = 0;
  }

  // // Prepare JSON document
  DynamicJsonDocument doc(1024);
  doc["time"] = location.time;
  doc["latitude"] = location.latitude;
  doc["nsh"] = String(location.NSH);
  doc["longitude"] = location.longitude;
  doc["ewh"] = String(location.EWH);
  doc["fixQuality"] = location.fixQuality;
  doc["connectedSats"] = location.connectedSats;
  doc["speed"] = location.speed;
  doc["date"] = location.date;
  doc["pressure"] = env_val.pressure;
  doc["temperature"] = env_val.temperature;
  doc["altitude"] = env_val.altitude;

  Serialize JSON document
  String json;
  serializeJson(doc, json);


//  String body = "{ \"time\": " + String(location.time) + " }";
 // Serial.println("sending json data: " + body);

  // Create http client object
  HTTPClient httpClient;

  // Send the http request
  httpClient.begin("http://192.168.43.141:4000/stats");
  httpClient.addHeader("Content-Type", "application/json");
  httpClient.POST(json);

  // calling the post method to send the data
  //int statuscode = httpClient.POST(body);

  // Read response
  Serial.print(httpClient.getString());
  //Serial.println("status code is " + String(statuscode));


  // Disconnect
  httpClient.end();
  
  // Resetting receive data flag
  receive_data_flag = 0;

  s.end();
  delay(1000);
}
