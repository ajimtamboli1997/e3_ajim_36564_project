#include "LPC17xx.h"
#include "uart.h"
#include <string.h>
#include <stdio.h>
#include "gps.h"
#include "bmp.h"
#include "i2c.h"
#include "spi.h"



typedef struct env_values{
	float pressure;
	float temperature;
	float altitude;
}env_t;


char gps_str[1024];
char location_data[256];
gps_data_t hc_location;
env_t hc_env_val;

env_t env_values;


void temp_function();


int main()
{
	SystemInit();

	uart3_init(9600);
	bmp_init();
	
	spi_init();
	

	while(1) {
		// enable RBR interrupt
		LPC_UART3->IER |= BV(IER_RBR);

		uart3_gets(gps_str);

		gps_data_t location = router(gps_str);

	//	T 171803.00, Lat 2002.44789, NSH N, Log 07428.52201, EWH E, qua 1, Consat 09, Spd 0.214, D 060121
	
		bmp_read(&env_values.pressure, &env_values.temperature, &env_values.altitude);

		sprintf(location_data,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%.2f\r\n", location.time, location.latitude, location.NSH, location.longitude, location.EWH, location.fixQuality, location.connectedSats, location.speed, location.date, env_values.pressure, env_values.temperature, env_values.altitude);

		uart3_puts(location_data);
		
		//char temp[2] = "3";

		//connnectedSat_led(atoi(temp));
		connnectedSat_led( atoi (location.connectedSats) );

//		temp_function();
		sw_delay(4000);

		spi_transfer(0x00);
	
		sw_delay(1000);

	}
	return 0;
}

void temp_function(){
		strcpy(hc_location.time, "171803.00");
		strcpy(hc_location.latitude, "2002.44789");
		strcpy(hc_location.NSH, "N");
		strcpy(hc_location.longitude, "07428.52201");
		strcpy(hc_location.EWH, "E");
		strcpy(hc_location.fixQuality, "1");
		strcpy(hc_location.connectedSats, "09");
		strcpy(hc_location.speed, "0.214");
		strcpy(hc_location.date, "080121");
		
		// hc_env_val.pressure = 1018.26;
		// hc_env_val.temperature = 25.06;
		// hc_env_val.altitude = 18.20;

		sprintf(location_data,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%.2f\r\n", hc_location.time, hc_location.latitude, hc_location.NSH, hc_location.longitude, hc_location.EWH, hc_location.fixQuality, hc_location.connectedSats, hc_location.speed, hc_location.date, env_values.pressure, env_values.temperature, env_values.altitude);

		uart3_puts(location_data);

		sw_delay(5000);

		strcpy(hc_location.time, "013503.00");
		strcpy(hc_location.latitude, "2002.44790");
		strcpy(hc_location.NSH, "N");
		strcpy(hc_location.longitude, "07428.52221");
		strcpy(hc_location.EWH, "E");
		strcpy(hc_location.fixQuality, "1");
		strcpy(hc_location.connectedSats, "12");
		strcpy(hc_location.speed, "0.128");
		strcpy(hc_location.date, "080121");

		// hc_env_val.pressure = 1018.28;
		// hc_env_val.temperature = 24.08;
		// hc_env_val.altitude = 18.50;

		sprintf(location_data,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%.2f\r\n", hc_location.time, hc_location.latitude, hc_location.NSH, hc_location.longitude, hc_location.EWH, hc_location.fixQuality, hc_location.connectedSats, hc_location.speed, hc_location.date, env_values.pressure, env_values.temperature, env_values.altitude);

		uart3_puts(location_data);

		sw_delay(5000);

		strcpy(hc_location.time, "013907.00");
		strcpy(hc_location.latitude, "2002.44791");
		strcpy(hc_location.NSH, "N");
		strcpy(hc_location.longitude, "07428.52220");
		strcpy(hc_location.EWH, "E");
		strcpy(hc_location.fixQuality, "1");
		strcpy(hc_location.connectedSats, "12");
		strcpy(hc_location.speed, "0.126");
		strcpy(hc_location.date, "080121");

		// hc_env_val.pressure = 1018.29;
		// hc_env_val.temperature = 24.04;
		// hc_env_val.altitude = 18.60;

		sprintf(location_data,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%.2f\r\n", hc_location.time, hc_location.latitude, hc_location.NSH, hc_location.longitude, hc_location.EWH, hc_location.fixQuality, hc_location.connectedSats, hc_location.speed, hc_location.date, env_values.pressure, env_values.temperature, env_values.altitude);

		uart3_puts(location_data);

		sw_delay(5000);
}