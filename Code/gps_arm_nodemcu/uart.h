#ifndef __UART_H
#define __UART_H

#include "LPC17xx.h"

#define LCR_DL0		0
#define LCR_DL1		1
#define LCR_STOP	2
#define LCR_PAREN	3
#define LCR_DLAB	7

#define LSR_RDR		0
#define LSR_THRE	5

#define FCR_EN		0

/*
Enables the Receive Data Available interrupt for UARTn. It also controls
the Character Receive Time-out interrupt.
0 Disable the RDA interrupts.
1 Enable the RDA interrupts.

Enables the THRE interrupt for UARTn. The status of this can be read
from UnLSR[5].
0 Disable the THRE interrupts.
1 Enable the THRE interrupts.

Enables the UARTn RX line status interrupts. The status of this interrupt
can be read from UnLSR[4:1].
0 Disable the RX line status interrupts.
1 Enable the RX line status interrupts.
*/
#define IER_RBR		0
#define IER_THRE	1
#define IER_RLS		2

#define IIR_PEND		0
#define IIR_IID(iir)	((iir >> 1) & 0x07) // 100x >> 1 = 100 & 111 = -----> 100 (THRE Empty)
#define IID_RLS			3
#define IID_RDA			2
#define IID_CTI			6
#define IID_THRE		1

void uart3_init(uint32_t baud);
void uart3_puts(char str[]);
int uart3_gets(char str[]);
void UART3_IRQHandler(void);

void uart0_init(uint32_t baud);
void uart0_puts(char str[]);
int uart0_gets(char str[]);
void UART0_IRQHandler(void);

#endif





