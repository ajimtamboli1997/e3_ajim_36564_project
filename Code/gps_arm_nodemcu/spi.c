#include "spi.h"

void spi_init(void) {
	// Enable SPI pins (P0.15, P0.17 & P0.18) and P0.16 as GPIO
	LPC_PINCON->PINSEL0 &= ~(BV(31) | BV(30));
	LPC_PINCON->PINSEL0 |= BV(31);
	LPC_PINCON->PINSEL1 &= ~(BV(5) | BV(4) | BV(3) | BV(2) | BV(1) | BV(0) );
	LPC_PINCON->PINSEL1 |= BV(3) | BV(5);

	// set SPI prescalar -- CPSR
	LPC_SSP0->CPSR = SSP_PR_VAL;

	// enable and configure SSP for SPI transfer -- CR0 & CR1
	LPC_SSP0->CR0 = ((SSPCR0_SCR_VAL-1)<<SSPCR0_SCR) | BV(SSPCR0_CPHA) | BV(SSPCR0_CPOL) | SSPCR0_8_BIT_DL;
	LPC_SSP0->CR1 = BV(SSPCR1_EN);

	// make P0.16 as output pin and disable slave
	LPC_GPIO0->FIODIR |= BV(SSEL);
	LPC_GPIO0->FIOSET |= BV(SSEL);
}

uint32_t spi_transfer(uint32_t data) {
	uint32_t sr, val;
	// enable slave
	LPC_GPIO0->FIOCLR = BV(SSEL);

	// clear status flags -- reading SR
	sr = LPC_SSP0->SR;
	(void)sr; // remove warning of unused
	// write the data on DR
	LPC_SSP0->DR = data;
	// wait for previous data to transfer - SR
	while( (LPC_SSP0->SR & BV(SPSR_RXFNE)) == 0)
		;
	// read the data from DR
	val = LPC_SSP0->DR;
	
	// disable slave
	LPC_GPIO0->FIOSET |= BV(SSEL);
	return val;
}

