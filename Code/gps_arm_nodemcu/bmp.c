#include "bmp.h"
#include "uart.h"

calib_t calib_values;
int32_t t_fine;

void bmp_init(void)
{
	uint8_t ctrl_meas = 0;
	uint8_t config = 0;

	// Forced mode. Comment the below two lines before using normal mode.
	// Needs set_forced_mode() to be called in bmp_read()
	ctrl_meas |= (FORCED_MODE | OSRS_P | OSRS_T);
	config |= FILTER_X4;

	// Uncomment below two lines for normal mode
	// Comment set_forced_mode() in bmp_read()
	// ctrl_meas |= (NORMAL_MODE | OSRS_P | OSRS_T);
	// config |= FILTER_X4 | T_STANDBY;

	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	i2c_send_data(BMP_CTRL_MEAS_REG_ADDR);
	i2c_send_data(ctrl_meas);
	i2c_send_data(BMP_CTRL_MEAS_REG_ADDR);
	i2c_send_data(config);
	i2c_stop();
	
	get_all_calib_values();
}

void set_forced_mode()
{
	uint8_t ctrl_meas = 0;
	ctrl_meas |= (FORCED_MODE | OSRS_P | OSRS_T);
	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	i2c_send_data(BMP_CTRL_MEAS_REG_ADDR);
	i2c_send_data(ctrl_meas);
	i2c_stop();
	
	get_all_calib_values();
}

void bmp_write(uint8_t addr, uint8_t byte)
{
	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	i2c_send_data(addr);
	i2c_send_data(byte);
	i2c_stop();
}

void bmp_read(float *pressure, float *temperature, float *altitude)
{
	uint8_t temporary;
	int32_t raw_temperature = 0;
	int32_t raw_pressure = 0;

	// Update Data registers by setting forced mode
	set_forced_mode();

	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	// Access pressure MSB register
	i2c_send_data(BMP_PRESS_MSB);
	// Repeated start
	// BMP_PRESS_LSB and BMP_PRESS_XLSB will be accessed by addr auto-increment feature
	i2c_rep_start();
	i2c_send_addr(BMP_R);
	// Read BMP_PRESS_MSB
	temporary = i2c_recv_ack();
	raw_pressure |= temporary << 12;
	// Read BMP_PRESS_LSB
	temporary = i2c_recv_ack();
	raw_pressure |= temporary << 4;
	// Read BMP_PRESS_XLSB
	temporary = i2c_recv_ack();
	temporary = temporary >> 4;
	raw_pressure |= temporary;
	// Read BMP_TEMP_MSB
	temporary = i2c_recv_ack();
	raw_temperature |= temporary << 12;
	// Read BMP_TEMP_LSB
	temporary = i2c_recv_ack();
	raw_temperature |= temporary << 4;
	// Read BMP_TEMP_XLSB
	temporary = i2c_recv_nack();
	temporary = temporary >> 4;
	raw_temperature |= temporary;

	*temperature = (float)bmp280_compensate_temp_int32(raw_temperature) / 100;
	*pressure = (float)(bmp280_compensate_pressure_int64(raw_pressure) >> 8) / 100;
	*altitude = (float)bmp280_compensate_Altitude_int32(pressure) ;

	i2c_stop();
}

int16_t fetch_calib_values(uint8_t addr){
	int16_t local_calib = 0;
	int16_t msb_calib = 0;

	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	// Access pressure MSB register
	i2c_send_data(addr);
	// Repeated start
	// BMP_PRESS_LSB and BMP_PRESS_XLSB will be accessed by addr auto-increment feature
	i2c_rep_start();
	i2c_send_addr(BMP_R);

	// Grab LSB
	local_calib |= i2c_recv_ack();
	// Grab MSB
	msb_calib = i2c_recv_nack();
	msb_calib = msb_calib << 8;
	local_calib |= msb_calib;

	i2c_stop();

	return local_calib;
}

void get_all_calib_values(void){
	memset(&calib_values, 0, sizeof(calib_t));

	calib_values.dig_t1 = (uint16_t)fetch_calib_values(dig_t1_lsb_addr);
	calib_values.dig_t2 = fetch_calib_values(dig_t2_lsb_addr);
	calib_values.dig_t3 = fetch_calib_values(dig_t3_lsb_addr);

	calib_values.dig_p1 = (uint16_t)fetch_calib_values(dig_p1_lsb_addr);
	calib_values.dig_p2 = fetch_calib_values(dig_p2_lsb_addr);
	calib_values.dig_p3 = fetch_calib_values(dig_p3_lsb_addr);
	calib_values.dig_p4 = fetch_calib_values(dig_p4_lsb_addr);
	calib_values.dig_p5 = fetch_calib_values(dig_p5_lsb_addr);
	calib_values.dig_p6 = fetch_calib_values(dig_p6_lsb_addr);
	calib_values.dig_p7 = fetch_calib_values(dig_p7_lsb_addr);
	calib_values.dig_p8 = fetch_calib_values(dig_p8_lsb_addr);
	calib_values.dig_p9 = fetch_calib_values(dig_p9_lsb_addr);

//	display_calib_values();
}

// void display_calib_values(void){
// 	char string[32];

// 	sprintf(string, "dig_t1 = %u\n", calib_values.dig_t1);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_t2 = %d\n", calib_values.dig_t2);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_t3 = %d\n", calib_values.dig_t3);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p1 = %u\n", calib_values.dig_p1);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p2 = %d\n", calib_values.dig_p2);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p3 = %d\n", calib_values.dig_p3);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p4 = %d\n", calib_values.dig_p4);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p5 = %d\n", calib_values.dig_p5);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p6 = %d\n", calib_values.dig_p6);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p7 = %d\n", calib_values.dig_p7);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p8 = %d\n", calib_values.dig_p8);
// 	uart_puts(string);
// 	uart_puts("\r\n");

// 	sprintf(string, "dig_p9 = %d\n", calib_values.dig_p9);
// 	uart_puts(string);
// 	uart_puts("\r\n");
// }

// Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23 DegC.
	// t_fine carries fine temperature as global value
int32_t bmp280_compensate_temp_int32(int32_t adc_T){
	int32_t var1, var2, T;
	var1 = ((((adc_T >> 3) - ((int32_t)calib_values.dig_t1 << 1))) * ((int32_t)calib_values.dig_t2)) >> 11;
	var2 = (((((adc_T >> 4) - ((int32_t)calib_values.dig_t1)) * ((adc_T >> 4) - ((int32_t)calib_values.dig_t1))) >> 12) * ((int32_t)calib_values.dig_t3)) >> 14;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) >> 8;
	return T;
}

// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
// Output value of “24674867” represents 24674867/256 = 96386.2 Pa = 963.862 hPa
uint32_t bmp280_compensate_pressure_int64(int32_t adc_P)
{
	int64_t var1, var2, p;
	var1 = ((int64_t)t_fine) - 128000;
	var2 = var1 * var1 * (int64_t)calib_values.dig_p6;
	var2 = var2 + ((var1 * (int64_t)calib_values.dig_p5) << 17);
	var2 = var2 + (((int64_t)calib_values.dig_p4) << 35);
	var1 = ((var1 * var1 * (int64_t)calib_values.dig_p3) >> 8) + ((var1 * (int64_t)calib_values.dig_p2) << 12);
	var1 = (((((int64_t)1) << 47) + var1)) * ((int64_t)calib_values.dig_p1) >> 33;
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	p = 1048576 - adc_P;
	p = (((p << 31) - var2) * 3125) / var1;
	var1 = (((int64_t)calib_values.dig_p9) * (p >> 13) * (p >> 13)) >> 25;
	var2 = (((int64_t)calib_values.dig_p8) * p) >> 19;
	p = ((p + var1 + var2) >> 8) + (((int64_t)calib_values.dig_p7) << 4);
	return (uint32_t)p;
}

float bmp280_compensate_Altitude_int32(float *pressure) {

  return (44330.0 * (1 - pow(*pressure / SEALEVEL, 0.1903)));
}