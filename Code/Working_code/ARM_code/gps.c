#include "gps.h"
#include "uart.h"
#include "spi.h"

gps_data_t location;
int gprmc_date_flag = 0;
int gpgga_numsv_flag = 0;


// Seperate all NMEA strings
void separate_nmea(char *nmea, char gp_string[][256])
{
    int i = 0;
    strcpy(gp_string[i], strtok(nmea, "$"));
    //printf("%s\n", gp_string[i]);
    i++;
    while (i < 8)
    {
        strcpy(gp_string[i], strtok(NULL, "$"));
        // printf("%s\n", gp_string[i]);
        i++;
    }
}

// inserting x where there are consecutive commas
void insert_x(char *gp_str)
{

    int j, k, i = 0;
    char temp[128];
    while (gp_str[i] != '\0')
    {

        if ((gp_str[i] == ',') && (gp_str[i + 1] == ','))
        {

            k = i + 1;
            j = 0;
            while (gp_str[k] != '\0')
            {
                temp[j] = gp_str[k];
                k++;
                j++;
            }
            temp[j] = '\0';

            gp_str[i + 1] = 'x';
            gp_str[i + 2] = '\0';

            strcat(gp_str, temp);
        }
        i++;
    }
}

gps_data_t router(char *nmea)
{

    char gp_string[8][256];

    separate_nmea(nmea, gp_string);

    insert_x(gp_string[0]);
    //    uart3_puts(gp_string[0]);

    insert_x(gp_string[2]);
    //  uart3_puts(gp_string[2]);

    strtok(gp_string[0], ",");
    gprmc(&location, gp_string[0]);

    strtok(gp_string[2], ",");
    gpgga(&location, gp_string[2]);

    return location;
}

// tokenizing gpgga string
void gpgga(gps_data_t *location, char *nmea)
{
    char *token;
    int i;

    i = 1;

    do
    {
        token = strtok(NULL, ",\r\n");

        switch (i)
        {
        case TIME:
            strcpy(location->time, token);
            break;

        case LATITUDE:
            strcpy(location->latitude, token);
            break;

        case NSH:
            strcpy(location->NSH, token);
            break;

        case LONGITUDE:
            strcpy(location->longitude, token);
            break;

        case EWH:
            strcpy(location->EWH, token);
            break;

        case QUALITY:
            strcpy(location->fixQuality, token);
            break;

        case NUMSV:
            gpgga_numsv_flag = 1;
            strcpy(location->connectedSats, token);
            break;

        default:
            break;
        }

        i++;
    } while (!gpgga_numsv_flag);
}

// tokenizing gprmc string
void gprmc(gps_data_t *location, char *nmea)
{
    char *token;
    int i;

    i = 1;

    while (!gprmc_date_flag)
    {
        token = strtok(NULL, ",\r\n");

        switch (i)
        {
        case STATUS:
            strcpy(location->status, token);
            break;

        case SPEED:
            strcpy(location->speed, token);
            break;

        case DATE:
            gprmc_date_flag = 1;
            strcpy(location->date, token);
            break;

        default:
            break;
        }
        i++;
    }
}

void connnectedSat_led(uint8_t spi_leds)
{
    switch (spi_leds)
    {
            case 0:
                spi_transfer(0x00);
                break;
            case 1:
                spi_transfer(0x80);
                break;
            case 2:
                spi_transfer(0xC0); 
                break;
            case 3:
                spi_transfer(0xE0); 
                break;
            case 4:
                spi_transfer(0xF0);
                break;
            case 5:
                spi_transfer(0xF8);
                break;
            case 6:
                spi_transfer(0xFC);
                break;
            case 7:
                spi_transfer(0xFE);
                break;
            case 8:
                spi_transfer(0xFF);
                break;
            default:
                spi_transfer(0xFF);
                break;
    }
}