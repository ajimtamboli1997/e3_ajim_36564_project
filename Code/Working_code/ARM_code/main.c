#include "LPC17xx.h"
#include "uart.h"
#include <string.h>
#include <stdio.h>
#include "gps.h"
#include "bmp.h"
#include "i2c.h"
#include "spi.h"

char gps_str[1024];
char location_data[256];
gps_data_t hc_location;
env_t hc_env_val;

env_t env_values;

void temp_function();

int main()
{

	uart3_init(9600);
	bmp_init();
	spi_init();

	while (1)
	{

		uart3_gets(gps_str);

		gps_data_t location = router(gps_str);

		//T 171803.00, Lat 2002.44789, NSH N, Log 07428.52201, EWH E, qua 1, Consat 09, Spd 0.214, D 060121

		bmp_read(&env_values.pressure, &env_values.temperature, &env_values.altitude);

		sprintf(location_data, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%.2f\r\n", location.time, location.latitude, location.NSH, location.longitude, location.EWH, location.fixQuality, location.connectedSats, location.speed, location.date, env_values.pressure, env_values.temperature, env_values.altitude);
		//printf("%s", location);
		uart3_puts(location_data);

		//connnectedSat_led(atoi(temp));
		connnectedSat_led(atoi(location.connectedSats));

		sw_delay(4000);

		spi_transfer(0x00);

		sw_delay(1000);
	}
	return 0;
}