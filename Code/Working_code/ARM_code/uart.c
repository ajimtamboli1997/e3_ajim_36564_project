#include "uart.h"
#include <stdio.h>

volatile static char *tx3_str = NULL;
volatile static char *rx3_str = NULL;
volatile static int tx3_index = 0;
static volatile int rx3_index = 0;
static volatile int tx3_completed = 1;
static volatile int rx3_completed = 0;

static volatile int rx0_index = 0;
static volatile int tx0_completed = 1;
static volatile int rx0_completed = 0;

static volatile int gpgll_flag = 0;

void uart3_init(uint32_t baud)
{
	uint16_t dl;
	// select P0.0 and P0.1 as UART3 pins -- PINSEL0
	LPC_PINCON->PINSEL0 &= ~(BV(3) | BV(2) | BV(1) | BV(0));
	LPC_PINCON->PINSEL0 |= BV(3) | BV(1);
	// enable fifo -- FCR
	LPC_UART3->FCR |= BV(FCR_EN);
	// configure UART: 1 stop bits, no parity, 8 bit data, enable divisor latch -- LCR
	LPC_UART3->LCR = BV(LCR_DLAB) | BV(LCR_DL1) | BV(LCR_DL0);
	// calculate divisor latch value and set -- DLL & DLM
	dl = (PCLK / 16) / baud;
	LPC_UART3->DLL = dl & 0x00FF;
	LPC_UART3->DLM = dl >> 8;
	// disable divisor latch -- LCR
	LPC_UART3->LCR &= ~BV(LCR_DLAB);
	// enable interrupt in peripheral
	//LPC_UART3->IER |= BV(IER_RBR) |  BV(IER_THRE);
	LPC_UART3->IER |= BV(IER_THRE);

	// enable interrupt in NVIC
	NVIC_EnableIRQ(UART3_IRQn);
}

void uart3_puts(char str[])
{
	// wait for previous string to be transmitted
	while (!tx3_completed)
		;
	tx3_completed = 0;
	// save address of the string globally (in tx_str)
	tx3_str = str;
	// send first character
	tx3_index = 0;
	LPC_UART3->THR = tx3_str[tx3_index];
	tx3_index++;
}

int uart3_gets(char str[])
{
	// SUNBEAM\r\n
	rx3_str = str;
	rx3_index = 0;
	rx3_completed = 0;

	// enable RBR interrupt
	LPC_UART3->IER |= BV(IER_RBR);

	while (rx3_completed == 0)
		;

	rx3_completed = 0;

	return rx3_index;
}


void UART3_IRQHandler(void)
{
	// read IIR (clear the interrupt)
	uint32_t iid, iir = LPC_UART3->IIR;
	// get interrupt id and handle the interrupt
	iid = IIR_IID(iir);
	switch (iid)
	{
	case IID_RLS:
		break;
	case IID_RDA:
		// rx3_str[rx3_index] = LPC_UART3->RBR;
		// rx3_index++;
		// if(rx3_str[rx3_index - 1] == '\r'){
		// 	rx3_str[rx3_index++] = '\n';
		// 	rx3_str[rx3_index] = '\0';
		// 	rx3_completed = 1;
		// }
		rx3_str[rx3_index] = LPC_UART3->RBR;
		rx3_index++;
		if (rx3_str[rx3_index - 1] == 'L')
		{
			gpgll_flag = 1;
			// rx0_str[rx0_index++] = '\n';
			// rx0_str[rx0_index] = '\0';
		}
		if ((rx3_str[rx3_index - 1] == '\n') && (gpgll_flag == 1))
		{
			rx3_completed = 1;
			gpgll_flag = 0;
			rx3_str[rx3_index] = '\0';
			// disable interrupt in peripheral
			LPC_UART3->IER &= ~BV(IER_RBR);
		}

		break;
	case IID_CTI:
		break;
	case IID_THRE: // if THRE interrupt, send next character
		if (tx3_str[tx3_index] != '\0')
		{
			LPC_UART3->THR = tx3_str[tx3_index];
			tx3_index++;
		}
		else
			tx3_completed = 1;
		break;
	}
}