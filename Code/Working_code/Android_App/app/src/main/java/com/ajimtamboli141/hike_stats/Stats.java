package com.ajimtamboli141.hike_stats;

public class Stats {
    private int id;
    private int time;
    private double latitude;
    private String nsh;
    private double longitude;
    private String ewh;
    private int fixQuality;
    private int connectedSats;
    private float speed;
    private int date;
    private float pressure;
    private float temperature;
    private float altitude;

    public Stats(int id, int time, double latitude, String nsh, double longitude, String ewh, int fixQuality, int connectedSats, float speed, int date, float pressure, float temperature, float altitude) {
        this.id = id;
        this.time = time;
        this.latitude = latitude;
        this.nsh = nsh;
        this.longitude = longitude;
        this.ewh = ewh;
        this.fixQuality = fixQuality;
        this.connectedSats = connectedSats;
        this.speed = speed;
        this.date = date;
        this.pressure = pressure;
        this.temperature = temperature;
        this.altitude = altitude;
    }

    public Stats() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getNsh() {
        return nsh;
    }

    public void setNsh(String nsh) {
        this.nsh = nsh;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getEwh() {
        return ewh;
    }

    public void setEwh(String ewh) {
        this.ewh = ewh;
    }

    public int getFixQuality() {
        return fixQuality;
    }

    public void setFixQuality(int fixQuality) {
        this.fixQuality = fixQuality;
    }

    public int getConnectedSats() {
        return connectedSats;
    }

    public void setConnectedSats(int connectedSats) {
        this.connectedSats = connectedSats;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }
}