# sudo pip3 install flask

from flask import Flask, request, jsonify
import mysql.connector
from pyfcm import FCMNotification

# Create a push service instance
# Grab the api key from console.firebase.google.com project settings
# TODO
push_service = FCMNotification(api_key="AAAApxTrZOM:APA91bH8CIvp8yyyixid1vmV_kkWFxzeN1IgqzIQ4pMw2EP1RQnPZmDJEBN_512B8vl5fQkFyrCpf5sVT3_X2EMkE3Pho5Y9ABeF4dk9BGqNOhfp6WIoJ-fcNp9ztGJPwLxzVyIFX99h")

# create a server process
app = Flask(__name__)


def open_connection():
  connection = mysql.connector.connect(host="localhost", user="root", password="Root", database="gps_db", port=3306)
  
  return connection


def execute_select_query(query):
  """
  this function is supposed to execute select query
  and return the output
  """
  connection = open_connection()
  
  cursor = connection.cursor()
  cursor.execute(query)

  data = cursor.fetchall()

  cursor.close()
  connection.close()
  
  return data


def execute_query(query):
  """
  this function is supposed to execute insert, update
  and delete queries and commit the connection
  """
  connection = open_connection()
  cursor = connection.cursor()
  cursor.execute(query)
  connection.commit()

  cursor.close()
  connection.close()


def create_response(data, error=None):
  # create empty dictionary
  result = dict()

  # check if there is any error
  if error == None:
    # there is no error to report
    result['status'] = 'success'  # Adding element key is status and value is success
    result['data'] = data         # Adding element key is data and value is data variable # We have added the list to dictionary

  else:
    # there is an error 
    result['status'] = 'error'

  return jsonify(result)
    

@app.route("/", methods=["GET"])
def welcome():
  return "Location and Environment Stats"


@app.route("/stats", methods=["GET"])
def get_stats():
  # query = "select id, temperature, collection_on from temperature_data order by id desc limit 10"
  query = "select id, time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude from gps_table order by id desc limit 10"
  
  gps_table = execute_select_query(query)
  print(gps_table)

  # Creating an empty list
  gps_values = []
  for (id, time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude) in gps_table:
    gps_values.append({
      # "id": id,
      # "temperature": temperature,
      # "collection_on": collection_on
      "id": id,
      "time": time,
      "latitude": latitude,
      "nsh": nsh,
      "longitude": longitude,
      "ewh": ewh,
      "fixQuality": fixQuality,
      "connectedSats": connectedSats,
      "status": status,
      "speed": speed,
      "date": date,
      "pressure": pressure,
      "temperature": temperature,
      "altitude": altitude
    })

  # Send the list to create_response
  return create_response(gps_values) # Jsonified dictionary will be returned to postman or android app that calls requests with GET method


@app.route('/stats', methods=["POST"])
def post_stats():
  # get the temperature value sent by NodeMCU
  time = request.json.get('time')                   # The NodeMCU sends jsonified data. Here we are extracting the values from the jsonified data
  latitude = request.json.get('latitude')
  nsh = request.json.get('nsh')
  longitude = request.json.get('longitude')
  ewh = request.json.get('ewh')
  fixQuality = request.json.get('fixQuality')
  connectedSats = request.json.get('connectedSats')
  status = 'A'
  speed = request.json.get('speed')
  date = request.json.get('date')
  pressure = request.json.get('pressure')
  temperature = request.json.get('temperature')
  altitude = request.json.get('altitude')
  
  query = f"insert into gps_table (time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, status, speed, date, pressure, temperature, altitude) values ('{time}', '{latitude}', '{nsh}', '{longitude}', '{ewh}', '{fixQuality}', '{connectedSats}', '{status}', '{speed}', '{date}', '{pressure}', '{temperature}', '{altitude}')"

  #query = f"insert into gps_table (time, latitude, longitude, fixQuality, connectedSats, speed, date, pressure, temperature, altitude) values ('{time}', '{latitude}', '{longitude}', '{fixQuality}', '{connectedSats}', '{speed}', '{date}', '{pressure}', '{temperature}', '{altitude}')"
   

  #query = f"insert into gps_table (time, latitude, longitude, fixQuality, connectedSats, speed, date, pressure, temperature, altitude) values ('{time}', '{latitude}', '{longitude}', '{fixQuality}', '{connectedSats}', '{speed}', '{date}', '{pressure}', '{temperature}', '{altitude}')"
 
  #query = f"insert into gps_table (time, latitude, nsh, longitude, ewh, fixQuality, connectedSats, speed, date, pressure, temperature, altitude) values ('{time}', '{latitude}', '{nsh}', '{longitude}', '{ewh}', '{fixQuality}', '{connectedSats}', '{speed}', '{date}', '{pressure}', '{temperature}', '{altitude}')"


  execute_query(query)

  # TODO
  registration_id = "ctTOtoP4RveUK8Cw2zqhdN:APA91bGVLrMSR0S1poENbrhuaRt8Dg_RzNHf4ZIos73dE9dg4UvGPE6qtTp5s3R9bHAvI2hmUyo6SeoiEFT8NRbKHXEayI-K0e5BuNW7v5kDRDUHIyabsE9M8_4eJSBvf1RtTknx64YC"
  message_title = "New GPS Stats"
  
  # Below commented code doesn't work
  # # Save new data in a dictionary
  # data = {
  #   "temperature": temperature
  # }
  # # Jsonify the dictonary and assign it to the message body
  # message_body = jsonify(data)

  # stats = {
  #   "time": time,
  #   "latitude": latitude,
  #   "nsh": nsh,
  #   "longitude": longitude,
  #   "ewh": ewh,
  #   "fixQuality": fixQuality,
  #   "connectedSats": connectedSats,
  #   "status": status,
  #   "speed": speed,
  #   "date": date,
  #   "pressure": pressure,
  #   "temperature": temperature,
  #   "altitude": altitude
  # }

  # print(stats)

  # TODO

  message_body = f"time: {time}, latitude: {latitude}, nsh: {nsh}, longitude: {longitude}, ewh: {ewh}, fixQuality: {fixQuality}, connectedSats: {connectedSats}, status: {status}, speed: {speed}, date: {date}, pressure: {pressure}, temperature: {temperature}, altitude: {altitude}"

  result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)

  print(f"result: {result}")
  
  return create_response('added stats') # This response is sent to postman or NodeMCU that uses the POST method



# start the server
app.run(host="0.0.0.0", debug=True, port=4000)
