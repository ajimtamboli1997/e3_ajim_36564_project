#include "gps.h"

gps_data_t location;
int gprmc_date_flag = 0;
int gpgga_numsv_flag = 0;

void display_gpgga(gps_data_t *location){
    printf("display_gpgga(): Time: %d, Latitude: %lf%c, Longitude: %lf%c, Fix quality: %d, Connected satellites: %d\n", location->time, location->latitude, location->NSH, location->longitude, location->EWH, location->fixQuality, location->connectedSats);
}

void display_gprmc(gps_data_t *location){
    printf("display_gprmc(): Status: %c, Speed: %lf, Date: %d\n", location->status, location->speed, location->date);
}

// Seperate all NMEA strings
void separate_nmea(char *nmea, char gp_string[][256])
{
    int i = 0;
    strcpy(gp_string[i], strtok(nmea, "$"));
    printf("%s\n", gp_string[i]);
    i++;
    while (i < 8)
    {
        strcpy(gp_string[i], strtok(NULL, "$"));
        printf("%s\n", gp_string[i]);
        i++;
    }
}

// inserting x where there are consecutive commas
void insert_x(char *gp_str)
{
    int j, k, i = 0;
    char temp[128];
    while (gp_str[i] != '\0')
    {

        if ((gp_str[i] == ',') && (gp_str[i + 1] == ','))
        {

            k = i + 1;
            j = 0;
            while (gp_str[k] != '\0')
            {
                temp[j] = gp_str[k];
                k++;
                j++;
            }
            temp[j] = '\0';

            gp_str[i + 1] = 'x';
            gp_str[i + 2] = '\0';

            strcat(gp_str, temp);
        }
        i++;
    }
}

gps_data_t router(char *nmea)
{

    char gp_string[8][256];

    separate_nmea(nmea, gp_string);

    insert_x(gp_string[0]);

    insert_x(gp_string[2]);

    strtok(gp_string[0],",");
    gprmc(&location, gp_string[0]);

    strtok(gp_string[2],",");
    gpgga(&location, gp_string[2]);

    return location;
}

// tokenizing gpgga string
void gpgga(gps_data_t *location, char *nmea)
{
    char *token;
    int i;

    i = 1;

    do
    {
        token = strtok(NULL, ",\r\n");

        switch (i)
        {
        case TIME:
            location->time = atoi(token);
            break;

        case LATITUDE:
            location->latitude = strtod(token, NULL);
            break;

        case NSH:
            location->NSH = *token;
            break;

        case LONGITUDE:
            location->longitude = strtod(token, NULL);
            break;

        case EWH:
            location->EWH = *token;
            break;

        case QUALITY:
            location->fixQuality = atoi(token);
            break;

        case NUMSV:
            gpgga_numsv_flag = 1;
            location->connectedSats = atoi(token);
            break;

        default:
            break;
        }

        i++;
    } while (!gpgga_numsv_flag);
}

// tokenizing gprmc string
void gprmc(gps_data_t *location, char *nmea)
{
    char *token;
    int i;

    i = 1;

    while (!gprmc_date_flag)
    {
        token = strtok(NULL, ",\r\n");

        switch (i)
        {
        case STATUS:
            location->status = *token;
            break;

        case SPEED:
            location->speed = strtod(token, NULL);
            break;

        case DATE:
            gprmc_date_flag = 1;
            location->date = atoi(token);
            break;

        default:
            break;
        }

        i++;
    }
}