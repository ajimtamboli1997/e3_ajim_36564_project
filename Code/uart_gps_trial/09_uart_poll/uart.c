#include "uart.h"

void uart_init(uint32_t baud) {
	uint16_t dl;
	// select P0.0 and P0.1 as UART3 pins -- PINSEL0
	LPC_PINCON->PINSEL0 &= ~(BV(3) | BV(2) | BV(1) | BV(0));
	LPC_PINCON->PINSEL0 |= BV(3) | BV(1);
	// enable fifo -- FCR
	LPC_UART3->FCR |= BV(FCR_EN);
	// configure UART: 2 stop bits, no parity, 8 bit data, enable divisor latch -- LCR
	LPC_UART3->LCR = BV(LCR_DLAB) | BV(LCR_DL1) | BV(LCR_DL0);
	// calculate divisor latch value and set -- DLL & DLM
	dl = (PCLK/16) / baud;
	LPC_UART3->DLL = dl & 0x00FF;
	LPC_UART3->DLM = dl >> 8;
	// disable divisor latch -- LCR
	LPC_UART3->LCR &= ~BV(LCR_DLAB);
}

void uart_putch(int ch) {
	// wait until previous char is transmitted -- LSR
	while( (LPC_UART3->LSR & BV(LSR_THRE)) == 0 )
		;
	// write the char -- THR
	LPC_UART3->THR = ch;
}

int uart_getch(void) {
	// wait until char is received -- LSR
	while( (LPC_UART3->LSR & BV(LSR_RDR)) == 0 )
		;
	// read the char -- RBR
	return (int)LPC_UART3->RBR;
}

void uart_puts(char str[]) {
	int i;
	for(i=0; str[i]!='\0'; i++)
		uart_putch(str[i]);
}

void uart_gets(char str[]) {
	int ch, i=0;
	while(ch != '\r'){
		ch = uart_getch();
		str[i++] = ch;
	}
	//str[i++] = '\n';
	//str[i] = '\0';
}
