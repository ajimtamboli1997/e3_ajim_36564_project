#include "tokenization.h"

gps_data_t location;

gps_data_t *router(char *nmea){
    //char nmea[128] = "$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,08,1.01,499.6,M,48.0,M,,*5B\n";
    //char nmea[128] = "$GPRMC,083559.00,A,4717.11437,N,00833.91522,E,0.004,77.52,091202,,,A*57";
    char *messageType;
    int messageTypeNum;

    // Getting the message ID for comparison
    messageType = strtok(nmea, "$,\r\n");
    // printf("main(): Message ID: %s\n", messageType);

    // Assigning correct number to messageTypeNum
    if(!strcmp(messageType, "GPGGA"))
        messageTypeNum = GPGGA;
    else if(!strcmp(messageType, "GPRMC"))
        messageTypeNum = GPRMC;

    // Calling the function corresponding to the message type
    switch (messageTypeNum)
    {
    case GPGGA:
        //printf("Entered GPGGA case.\n");
        gpgga(&location, nmea);
        // display_gpgga(&location);
        break;

    case GPRMC:
        //printf("Entered GPRMC case.\n");
        gprmc(&location, nmea);
        // display_gprmc(&location);
        break;
    
    default:
        //printf("Invalid Message ID Number.\n");
        break;
    }
    
    return &location;
}

void gpgga(gps_data_t *location, char *nmea){
    char *token;
    int i;

    i = 1;

    do {
        token = strtok(NULL, "$,\r\n");

        switch (i)
        {
        case TIME:
            location->time = atoi(token);
            break;

        case LATITUDE:
            location->latitude = strtod(token, NULL);
            break;

        case NSH:
            location->NSH = *token;
            break;

        case LOGITUDE:
            location->longitude = strtod(token, NULL);
            break;

        case EWH:
            location->EWH = *token;
            break;

        case QUALITY:
            location->fixQuality = atoi(token);
            break;

        case NUMSV:
            location->connectedSats = atoi(token);
            break;

        default:
            //printf("gpgga(): Invalid case.\n");
            break;
        }

        i++;
    }while(i <= 7);
}

void gprmc(gps_data_t *location, char *nmea){
    char *token;
    int i;

    i = 1;

    do{
        token = strtok(NULL, "$,\r\n");
        
        switch (i)
        {
        case STATUS:
            location->status = *token;
            break;

        case SPEED:
            location->speed = strtod(token, NULL);
            break;

        case DATE:
            location->date = atoi(token);
            break;
        
        default:
            break;
        }

        i++;
    }while(i <= 9);
}