#include "LPC17xx.h"
#include "uart.h"
#include <string.h>
#include <stdio.h>
#include "lcd.h"
#include "gps.h"

int main()
{
	char str[32];
	char gps_line1[16];
	char gps_line2[16];
	gps_data_t *neo;
	lcd_init();
	uart_init(9600);
	
	lcd_puts(LCD_LINE1, "GPS to LCD");
	while(1) {
		uart_gets(str);
		neo = router(str);
		sprintf(gps_line1, "Lat: %.2lf", neo->latitude);
		sprintf(gps_line2, "Long: %.2lf", neo->longitude);
		lcd_puts(LCD_LINE1, gps_line1);
		lcd_puts(LCD_LINE2, gps_line2);
		delay_ms(1000);
	}
	return 0;
}

