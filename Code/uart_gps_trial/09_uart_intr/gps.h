#ifndef __GPS_H
#define __GPS_H
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// 0        1           2           3    4              5   6           7       8       9       10  11      12  13          14          15  16
// $xxGGA,  time,       lat,        NS,  long,          EW, quality,    numSV,  HDOP,   alt,    M,  sep,    M,  diffAge,    diffStation *cs <CR><LF>
// $GPGGA,  092725.00,  4717.11399, N,   00833.91590,   E,  1,          08,     1.01,   499.6,  M,  48.0,   M,  ,                       *5B

// time - UTC Time
// lat - latitude
// NS - North / South Indicator
// long - longitude
// EW - East West Indicator
// 

// quality - Quality indicator for position fix, see table below and position fix flags description
// 0 - No Fix / Invalid
// 1 - Standard GPS (2D/3D)
// 2 - Differential GPS
// 6 - Estimated (DR) Fix

// numSV - Number of satellites used (range: 0-12)
// <CR><LF> - Carriage return and line feed

// 0        1           2       3           4   5               6   7       8       9       10  11      12      13  14
// $xxRMC,  time,       status, lat,        NS, long,           EW, spd,    cog,    date,   mv, mvEW,   posMode *cs <CR><LF>
// $GPRMC,  083559.00,  A,      4717.11437, N,  00833.91522,    E,  0.004,  77.52,  091202, ,   ,       A       *57

// status - Status, V = Navigation receiver warning, A = Data valid
// spd - Speed over ground
// date - Date in day, month, year format

typedef struct gps_data{
    int time;
    double latitude;
    char NSH; // Northern or Southern Hemisphere
    double longitude;
    char EWH; // Eastern or Western Hemisphere
    int fixQuality;
    int connectedSats; // Number of satellites connected
    char status;
    double speed;
    int date;
}gps_data_t;

extern gps_data_t location;

typedef enum messageID{
    GPGGA, GPRMC
}messageID_t;

typedef enum ggaField{
    TIME = 1, LATITUDE = 2, NSH = 3, LOGITUDE = 4, EWH = 5, QUALITY = 6, NUMSV = 7
}ggaField_t;

typedef enum rmcField{
    STATUS = 2, SPEED = 7, DATE = 9
}rmcField_t;

gps_data_t *router(char *nmea);
void gpgga(gps_data_t *location, char *nmea);
void gprmc(gps_data_t *location, char *nmea);

#endif