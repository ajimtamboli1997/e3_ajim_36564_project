#include "bmp.h"
#include "uart.h"

void bmp_init(void)
{
	uint8_t ctrl_meas = 0;
	uint8_t config = 0;
	ctrl_meas |= NORMAL_MODE | OSRS_P | OSRS_T;
	config |= FILTER_X4 | T_STANDBY;

	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	i2c_send_data(BMP_CTRL_MEAS_REG_ADDR);
	i2c_send_data(ctrl_meas);
	i2c_send_data(BMP_CTRL_MEAS_REG_ADDR);
	i2c_send_data(config);
	i2c_stop();
	uart_puts("BMP280 initialized\r\n");
}

void bmp_write(uint8_t addr, uint8_t byte)
{
	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	i2c_send_data(addr);
	i2c_send_data(byte);
	i2c_stop();
}

void bmp_read(uint32_t *pre, uint32_t *temp)
{
	uint8_t temporary = 0;
	uint32_t pressure = 0, temperature = 0;
	i2c_init();
	i2c_start();
	i2c_send_addr(BMP_W);
	// Access pressure MSB register
	i2c_send_data(BMP_PRESS_MSB);
	// Repeated start
	// BMP_PRESS_LSB and BMP_PRESS_XLSB will be accessed by addr auto-increment feature
	i2c_start();
	i2c_send_addr(BMP_R);
	// Read BMP_PRESS_MSB
	temporary = i2c_recv_ack();
	pressure |= temporary<<12;
	// Read BMP_PRESS_LSB
	temporary = i2c_recv_ack();
	pressure |= temporary<<4;
	// Read BMP_PRESS_XLSB
	temporary = i2c_recv_ack();
	temporary = temporary>>4;
	pressure |= temporary;
	
	// setting the out parameter
	*pre = pressure;

	// Read BMP_TEMP_MSB
	temporary = i2c_recv_ack();
	temperature |= temporary<<12;
	// Read BMP_TEMP_LSB
	temporary = i2c_recv_ack();
	temperature |= temporary<<4;
	// Read BMP_TEMP_XLSB
	temporary = i2c_recv_nack();
	temporary = temporary>>4;
	temperature |= temporary;

	// setting the out parameter
	*temp = temperature;

	i2c_stop();
}

