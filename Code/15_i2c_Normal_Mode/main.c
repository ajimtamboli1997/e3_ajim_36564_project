#include "LPC17xx.h"
#include "uart.h"
#include "bmp.h"
#include <stdio.h>

uint32_t pressure;
uint32_t temperature;

char string[32];
int main()
{
	SystemInit();
	uart_init(9600);
	uart_puts("I2C BMP DEMO!\r\n");
	bmp_init();
	uart_puts("Reading values from BMP280.\r\n");
	while(1)
	{
		bmp_read(&pressure, &temperature);
		sprintf(string, "Pressure: %lu and Temperature: %lu", pressure, temperature);
		uart_puts(string);
		uart_puts("\r\n");
		sw_delay(1000);
	}
}

