#include "LPC17xx.h"
#include "uart.h"
#include <string.h>
#include <stdio.h>
#include "gps.h"

int main()
{
	char str[32];
	char gps_str[1024];
	char location_data[256];
	int count = 0;
	// gps_data_t *neo;
//	uart0_init(9600);
	uart3_init(9600);
	
	while(1) {
		// uart3_puts("GPS Program\r\n");
		count = uart3_gets(gps_str);
	//	sprintf(str, "length of NMEA string is %d\r\n", count);
	//	uart3_puts(str);
	//	uart3_puts(gps_str);

		gps_data_t location = router(gps_str);

		sprintf(location_data,"T %s, Lat %s, NSH %s, Log %s, EWH %s, qua %s, Consat %s, Spd %s, D %s\r\n", location.time, location.latitude, location.NSH, location.longitude, location.EWH, location.fixQuality, location.connectedSats, location.speed, location.date);

		//sprintf(location_data,"T %d, Lat %0.2f, NSH %c, Log %0.2f, EWH %c, qua %d, Consat %d, S %c, Spd %0.2f, D %d\r\n", location.time, location.latitude, location.NSH, location.longitude, location.EWH, location.fixQuality, location.connectedSats, location.status, location.speed, location.date);
		//sprintf(location_data,"lat = %f",location.latitude);
		uart3_puts(location_data);
		// neo = router(str);
		// sprintf(gps_str, "Lat %.2lf and Long %.2lf", neo->latitude, neo->longitude);
		sw_delay(1000);
	}
	return 0;
}

