#ifndef __BMP_H
#define __BMP_H

#include "LPC17xx.h"
#include "i2c.h"

#define BMP_W	0xEC
#define BMP_R	0xED

/*
    If the value 0xB6 is written to the register,
    the device is reset using the complete power-on-reset
    procedure. Writing other values than 0xB6 has no effect.
*/
#define BMP_RESET_REG_ADDR       0xE0
#define BMP_RESET_VAL            0xB6



/*  
    MEAS-
    Automatically set to ‘1’ whenever a conversion is
    running and back to ‘0’ when the results have been
    transferred to the data registers.
*/
/*
    NVM-
    Automatically set to ‘1’ when the NVM data are being 
    copied to image registers and back to ‘0’ when the
    copying is done. The data are copied at power-on-reset
    and before every conversion.
*/
#define BMP_STATUS_REG_ADDR      0xF3
#define BMP_STATUS_MEAS          3
#define BMP_STATUS_NVM           0


/*
    Bit 7, 6, 5 osrs_t[2:0] Controls oversampling of temperature data.
    Bit 4, 3, 2 osrs_p[2:0] Controls oversampling of pressure data.
    Bit 1, 0 mode[1:0] Controls the power mode of the device.
*/
#define BMP_CTRL_MEAS_REG_ADDR   0xF4
// Power mode - Forced - mode[1:0] - Should be 01 or 10
// Power mode - Normal - mode[1:0] - Should be 11
// Power mode - Sleep - mode[1:0] - Should be 00
#define FORCED_MODE              1
#define SLEEP_MODE               0
#define NORMAL_MODE              3

// osrs_p [4:2]- x16 - 101
//#define OSRS_P  (1<<2)
#define OSRS_P  (5<<2)


// osrs_t [7:5]- x2  - 010
//#define OSRS_T  (1<<5)
#define OSRS_T  (2<<5)

/*
    Bit 7, 6, 5 t_sb[2:0] Controls inactive duration tstandby in normal mode.
    Bit 4, 3, 2 filter[2:0] Controls the time constant of the IIR filter.
    Bit 0 spi3w_en[0] Enables 3-wire SPI interface when set to ‘1’.
*/
#define BMP_CONFIG      0xF5
// Filtering level for sensor data.
// 4x filtering
#define FILTER_X4 0x02
#define T_STANDBY (1<<6)


// The “press” register contains the raw pressure measurement output data up[19:0].
// 0xF7 press_msb[7:0] Contains the MSB part up[19:12] of the raw pressure measurement output data.
#define BMP_PRESS_MSB   0xF7
// 0xF8 press_lsb[7:0] Contains the LSB part up[11:4] of the raw pressure measurement output data.
#define BMP_PRESS_LSB   0xF8
/*
0xF9 (bit 7, 6, 5, 4) press_xlsb[3:0] Contains the XLSB part up[3:0] of the raw
pressure measurement output data. Contents depend on temperature resolution.
*/
#define BMP_PRESS_XLSB  0xF9



// The “temp” register contains the raw temperature measurement output data ut[19:0].
// 0xFA temp_msb[7:0] Contains the MSB part ut[19:12] of the raw temperature measurement output data.
#define BMP_TEMP_MSB    0xFA
// 0xFB temp_lsb[7:0] Contains the LSB part ut[11:4] of the raw temperature measurement output data.
#define BMP_TEMP_LSB    0xFB
/*
0xFC (bit 7, 6, 5, 4) temp_xlsb[3:0] Contains the XLSB part ut[3:0] of the raw
temperature measurement output data. Contents depend on pressure resolution.
*/
#define BMP_TEMP_XLSB   0xFC

/*-------------------- Global Variables --------------------*/
//extern uint32_t pressure;
//extern uint32_t temperature;

/*-------------------- Function Declarations --------------------*/
void bmp_init(void);
void bmp_write(uint8_t addr, uint8_t byte);
void bmp_read();
void call_force_again(void);

#endif


